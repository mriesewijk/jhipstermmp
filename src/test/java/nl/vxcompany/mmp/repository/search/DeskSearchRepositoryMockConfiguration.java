package nl.vxcompany.mmp.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of DeskSearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class DeskSearchRepositoryMockConfiguration {

    @MockBean
    private DeskSearchRepository mockDeskSearchRepository;

}
