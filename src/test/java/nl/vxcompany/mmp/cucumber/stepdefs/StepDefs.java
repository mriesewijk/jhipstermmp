package nl.vxcompany.mmp.cucumber.stepdefs;

import nl.vxcompany.mmp.JhipstermmpApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = JhipstermmpApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
