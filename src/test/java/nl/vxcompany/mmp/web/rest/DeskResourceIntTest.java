package nl.vxcompany.mmp.web.rest;

import nl.vxcompany.mmp.JhipstermmpApp;

import nl.vxcompany.mmp.domain.Desk;
import nl.vxcompany.mmp.repository.DeskRepository;
import nl.vxcompany.mmp.repository.search.DeskSearchRepository;
import nl.vxcompany.mmp.service.DeskService;
import nl.vxcompany.mmp.service.dto.DeskDTO;
import nl.vxcompany.mmp.service.mapper.DeskMapper;
import nl.vxcompany.mmp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;


import static nl.vxcompany.mmp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DeskResource REST controller.
 *
 * @see DeskResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JhipstermmpApp.class)
public class DeskResourceIntTest {

    private static final String DEFAULT_DESK_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DESK_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_QR_CODE = "AAAAAAAAAA";
    private static final String UPDATED_QR_CODE = "BBBBBBBBBB";

    @Autowired
    private DeskRepository deskRepository;


    @Autowired
    private DeskMapper deskMapper;
    

    @Autowired
    private DeskService deskService;

    /**
     * This repository is mocked in the nl.vxcompany.mmp.repository.search test package.
     *
     * @see nl.vxcompany.mmp.repository.search.DeskSearchRepositoryMockConfiguration
     */
    @Autowired
    private DeskSearchRepository mockDeskSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDeskMockMvc;

    private Desk desk;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DeskResource deskResource = new DeskResource(deskService);
        this.restDeskMockMvc = MockMvcBuilders.standaloneSetup(deskResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Desk createEntity(EntityManager em) {
        Desk desk = new Desk()
            .deskName(DEFAULT_DESK_NAME)
            .qrCode(DEFAULT_QR_CODE);
        return desk;
    }

    @Before
    public void initTest() {
        desk = createEntity(em);
    }

    @Test
    @Transactional
    public void createDesk() throws Exception {
        int databaseSizeBeforeCreate = deskRepository.findAll().size();

        // Create the Desk
        DeskDTO deskDTO = deskMapper.toDto(desk);
        restDeskMockMvc.perform(post("/api/desks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deskDTO)))
            .andExpect(status().isCreated());

        // Validate the Desk in the database
        List<Desk> deskList = deskRepository.findAll();
        assertThat(deskList).hasSize(databaseSizeBeforeCreate + 1);
        Desk testDesk = deskList.get(deskList.size() - 1);
        assertThat(testDesk.getDeskName()).isEqualTo(DEFAULT_DESK_NAME);
        assertThat(testDesk.getQrCode()).isEqualTo(DEFAULT_QR_CODE);

        // Validate the Desk in Elasticsearch
        verify(mockDeskSearchRepository, times(1)).save(testDesk);
    }

    @Test
    @Transactional
    public void createDeskWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = deskRepository.findAll().size();

        // Create the Desk with an existing ID
        desk.setId(1L);
        DeskDTO deskDTO = deskMapper.toDto(desk);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeskMockMvc.perform(post("/api/desks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deskDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Desk in the database
        List<Desk> deskList = deskRepository.findAll();
        assertThat(deskList).hasSize(databaseSizeBeforeCreate);

        // Validate the Desk in Elasticsearch
        verify(mockDeskSearchRepository, times(0)).save(desk);
    }

    @Test
    @Transactional
    public void getAllDesks() throws Exception {
        // Initialize the database
        deskRepository.saveAndFlush(desk);

        // Get all the deskList
        restDeskMockMvc.perform(get("/api/desks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(desk.getId().intValue())))
            .andExpect(jsonPath("$.[*].deskName").value(hasItem(DEFAULT_DESK_NAME.toString())))
            .andExpect(jsonPath("$.[*].qrCode").value(hasItem(DEFAULT_QR_CODE.toString())));
    }
    

    @Test
    @Transactional
    public void getDesk() throws Exception {
        // Initialize the database
        deskRepository.saveAndFlush(desk);

        // Get the desk
        restDeskMockMvc.perform(get("/api/desks/{id}", desk.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(desk.getId().intValue()))
            .andExpect(jsonPath("$.deskName").value(DEFAULT_DESK_NAME.toString()))
            .andExpect(jsonPath("$.qrCode").value(DEFAULT_QR_CODE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingDesk() throws Exception {
        // Get the desk
        restDeskMockMvc.perform(get("/api/desks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDesk() throws Exception {
        // Initialize the database
        deskRepository.saveAndFlush(desk);

        int databaseSizeBeforeUpdate = deskRepository.findAll().size();

        // Update the desk
        Desk updatedDesk = deskRepository.findById(desk.getId()).get();
        // Disconnect from session so that the updates on updatedDesk are not directly saved in db
        em.detach(updatedDesk);
        updatedDesk
            .deskName(UPDATED_DESK_NAME)
            .qrCode(UPDATED_QR_CODE);
        DeskDTO deskDTO = deskMapper.toDto(updatedDesk);

        restDeskMockMvc.perform(put("/api/desks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deskDTO)))
            .andExpect(status().isOk());

        // Validate the Desk in the database
        List<Desk> deskList = deskRepository.findAll();
        assertThat(deskList).hasSize(databaseSizeBeforeUpdate);
        Desk testDesk = deskList.get(deskList.size() - 1);
        assertThat(testDesk.getDeskName()).isEqualTo(UPDATED_DESK_NAME);
        assertThat(testDesk.getQrCode()).isEqualTo(UPDATED_QR_CODE);

        // Validate the Desk in Elasticsearch
        verify(mockDeskSearchRepository, times(1)).save(testDesk);
    }

    @Test
    @Transactional
    public void updateNonExistingDesk() throws Exception {
        int databaseSizeBeforeUpdate = deskRepository.findAll().size();

        // Create the Desk
        DeskDTO deskDTO = deskMapper.toDto(desk);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDeskMockMvc.perform(put("/api/desks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deskDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Desk in the database
        List<Desk> deskList = deskRepository.findAll();
        assertThat(deskList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Desk in Elasticsearch
        verify(mockDeskSearchRepository, times(0)).save(desk);
    }

    @Test
    @Transactional
    public void deleteDesk() throws Exception {
        // Initialize the database
        deskRepository.saveAndFlush(desk);

        int databaseSizeBeforeDelete = deskRepository.findAll().size();

        // Get the desk
        restDeskMockMvc.perform(delete("/api/desks/{id}", desk.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Desk> deskList = deskRepository.findAll();
        assertThat(deskList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Desk in Elasticsearch
        verify(mockDeskSearchRepository, times(1)).deleteById(desk.getId());
    }

    @Test
    @Transactional
    public void searchDesk() throws Exception {
        // Initialize the database
        deskRepository.saveAndFlush(desk);
        when(mockDeskSearchRepository.search(queryStringQuery("id:" + desk.getId())))
            .thenReturn(Collections.singletonList(desk));
        // Search the desk
        restDeskMockMvc.perform(get("/api/_search/desks?query=id:" + desk.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(desk.getId().intValue())))
            .andExpect(jsonPath("$.[*].deskName").value(hasItem(DEFAULT_DESK_NAME.toString())))
            .andExpect(jsonPath("$.[*].qrCode").value(hasItem(DEFAULT_QR_CODE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Desk.class);
        Desk desk1 = new Desk();
        desk1.setId(1L);
        Desk desk2 = new Desk();
        desk2.setId(desk1.getId());
        assertThat(desk1).isEqualTo(desk2);
        desk2.setId(2L);
        assertThat(desk1).isNotEqualTo(desk2);
        desk1.setId(null);
        assertThat(desk1).isNotEqualTo(desk2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeskDTO.class);
        DeskDTO deskDTO1 = new DeskDTO();
        deskDTO1.setId(1L);
        DeskDTO deskDTO2 = new DeskDTO();
        assertThat(deskDTO1).isNotEqualTo(deskDTO2);
        deskDTO2.setId(deskDTO1.getId());
        assertThat(deskDTO1).isEqualTo(deskDTO2);
        deskDTO2.setId(2L);
        assertThat(deskDTO1).isNotEqualTo(deskDTO2);
        deskDTO1.setId(null);
        assertThat(deskDTO1).isNotEqualTo(deskDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(deskMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(deskMapper.fromId(null)).isNull();
    }
}
