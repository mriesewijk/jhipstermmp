/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { JhipstermmpTestModule } from '../../../test.module';
import { RoomMySuffixDeleteDialogComponent } from 'app/entities/room-my-suffix/room-my-suffix-delete-dialog.component';
import { RoomMySuffixService } from 'app/entities/room-my-suffix/room-my-suffix.service';

describe('Component Tests', () => {
    describe('RoomMySuffix Management Delete Component', () => {
        let comp: RoomMySuffixDeleteDialogComponent;
        let fixture: ComponentFixture<RoomMySuffixDeleteDialogComponent>;
        let service: RoomMySuffixService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JhipstermmpTestModule],
                declarations: [RoomMySuffixDeleteDialogComponent]
            })
                .overrideTemplate(RoomMySuffixDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RoomMySuffixDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RoomMySuffixService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
