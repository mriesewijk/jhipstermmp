/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhipstermmpTestModule } from '../../../test.module';
import { RoomMySuffixComponent } from 'app/entities/room-my-suffix/room-my-suffix.component';
import { RoomMySuffixService } from 'app/entities/room-my-suffix/room-my-suffix.service';
import { RoomMySuffix } from 'app/shared/model/room-my-suffix.model';

describe('Component Tests', () => {
    describe('RoomMySuffix Management Component', () => {
        let comp: RoomMySuffixComponent;
        let fixture: ComponentFixture<RoomMySuffixComponent>;
        let service: RoomMySuffixService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JhipstermmpTestModule],
                declarations: [RoomMySuffixComponent],
                providers: []
            })
                .overrideTemplate(RoomMySuffixComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RoomMySuffixComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RoomMySuffixService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new RoomMySuffix(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.rooms[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
