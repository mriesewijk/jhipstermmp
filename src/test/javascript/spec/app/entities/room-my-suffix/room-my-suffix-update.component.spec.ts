/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { JhipstermmpTestModule } from '../../../test.module';
import { RoomMySuffixUpdateComponent } from 'app/entities/room-my-suffix/room-my-suffix-update.component';
import { RoomMySuffixService } from 'app/entities/room-my-suffix/room-my-suffix.service';
import { RoomMySuffix } from 'app/shared/model/room-my-suffix.model';

describe('Component Tests', () => {
    describe('RoomMySuffix Management Update Component', () => {
        let comp: RoomMySuffixUpdateComponent;
        let fixture: ComponentFixture<RoomMySuffixUpdateComponent>;
        let service: RoomMySuffixService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JhipstermmpTestModule],
                declarations: [RoomMySuffixUpdateComponent]
            })
                .overrideTemplate(RoomMySuffixUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RoomMySuffixUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RoomMySuffixService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RoomMySuffix(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.room = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RoomMySuffix();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.room = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
