/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipstermmpTestModule } from '../../../test.module';
import { RoomMySuffixDetailComponent } from 'app/entities/room-my-suffix/room-my-suffix-detail.component';
import { RoomMySuffix } from 'app/shared/model/room-my-suffix.model';

describe('Component Tests', () => {
    describe('RoomMySuffix Management Detail Component', () => {
        let comp: RoomMySuffixDetailComponent;
        let fixture: ComponentFixture<RoomMySuffixDetailComponent>;
        const route = ({ data: of({ room: new RoomMySuffix(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JhipstermmpTestModule],
                declarations: [RoomMySuffixDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(RoomMySuffixDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RoomMySuffixDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.room).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
