/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { JhipstermmpTestModule } from '../../../test.module';
import { DeskMySuffixUpdateComponent } from 'app/entities/desk-my-suffix/desk-my-suffix-update.component';
import { DeskMySuffixService } from 'app/entities/desk-my-suffix/desk-my-suffix.service';
import { DeskMySuffix } from 'app/shared/model/desk-my-suffix.model';

describe('Component Tests', () => {
    describe('DeskMySuffix Management Update Component', () => {
        let comp: DeskMySuffixUpdateComponent;
        let fixture: ComponentFixture<DeskMySuffixUpdateComponent>;
        let service: DeskMySuffixService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JhipstermmpTestModule],
                declarations: [DeskMySuffixUpdateComponent]
            })
                .overrideTemplate(DeskMySuffixUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DeskMySuffixUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DeskMySuffixService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DeskMySuffix(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.desk = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DeskMySuffix();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.desk = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
