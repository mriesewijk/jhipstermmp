/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipstermmpTestModule } from '../../../test.module';
import { DeskMySuffixDetailComponent } from 'app/entities/desk-my-suffix/desk-my-suffix-detail.component';
import { DeskMySuffix } from 'app/shared/model/desk-my-suffix.model';

describe('Component Tests', () => {
    describe('DeskMySuffix Management Detail Component', () => {
        let comp: DeskMySuffixDetailComponent;
        let fixture: ComponentFixture<DeskMySuffixDetailComponent>;
        const route = ({ data: of({ desk: new DeskMySuffix(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JhipstermmpTestModule],
                declarations: [DeskMySuffixDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(DeskMySuffixDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DeskMySuffixDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.desk).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
