/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhipstermmpTestModule } from '../../../test.module';
import { DeskMySuffixComponent } from 'app/entities/desk-my-suffix/desk-my-suffix.component';
import { DeskMySuffixService } from 'app/entities/desk-my-suffix/desk-my-suffix.service';
import { DeskMySuffix } from 'app/shared/model/desk-my-suffix.model';

describe('Component Tests', () => {
    describe('DeskMySuffix Management Component', () => {
        let comp: DeskMySuffixComponent;
        let fixture: ComponentFixture<DeskMySuffixComponent>;
        let service: DeskMySuffixService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JhipstermmpTestModule],
                declarations: [DeskMySuffixComponent],
                providers: []
            })
                .overrideTemplate(DeskMySuffixComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DeskMySuffixComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DeskMySuffixService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new DeskMySuffix(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.desks[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
