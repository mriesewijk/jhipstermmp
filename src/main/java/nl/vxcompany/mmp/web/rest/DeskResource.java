package nl.vxcompany.mmp.web.rest;

import com.codahale.metrics.annotation.Timed;
import nl.vxcompany.mmp.service.DeskService;
import nl.vxcompany.mmp.web.rest.errors.BadRequestAlertException;
import nl.vxcompany.mmp.web.rest.util.HeaderUtil;
import nl.vxcompany.mmp.service.dto.DeskDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Desk.
 */
@RestController
@RequestMapping("/api")
public class DeskResource {

    private final Logger log = LoggerFactory.getLogger(DeskResource.class);

    private static final String ENTITY_NAME = "desk";

    private final DeskService deskService;

    public DeskResource(DeskService deskService) {
        this.deskService = deskService;
    }

    /**
     * POST  /desks : Create a new desk.
     *
     * @param deskDTO the deskDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new deskDTO, or with status 400 (Bad Request) if the desk has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/desks")
    @Timed
    public ResponseEntity<DeskDTO> createDesk(@RequestBody DeskDTO deskDTO) throws URISyntaxException {
        log.debug("REST request to save Desk : {}", deskDTO);
        if (deskDTO.getId() != null) {
            throw new BadRequestAlertException("A new desk cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DeskDTO result = deskService.save(deskDTO);
        return ResponseEntity.created(new URI("/api/desks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /desks : Updates an existing desk.
     *
     * @param deskDTO the deskDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated deskDTO,
     * or with status 400 (Bad Request) if the deskDTO is not valid,
     * or with status 500 (Internal Server Error) if the deskDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/desks")
    @Timed
    public ResponseEntity<DeskDTO> updateDesk(@RequestBody DeskDTO deskDTO) throws URISyntaxException {
        log.debug("REST request to update Desk : {}", deskDTO);
        if (deskDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DeskDTO result = deskService.save(deskDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, deskDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /desks : get all the desks.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of desks in body
     */
    @GetMapping("/desks")
    @Timed
    public List<DeskDTO> getAllDesks() {
        log.debug("REST request to get all Desks");
        return deskService.findAll();
    }

    /**
     * GET  /desks/:id : get the "id" desk.
     *
     * @param id the id of the deskDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the deskDTO, or with status 404 (Not Found)
     */
    @GetMapping("/desks/{id}")
    @Timed
    public ResponseEntity<DeskDTO> getDesk(@PathVariable Long id) {
        log.debug("REST request to get Desk : {}", id);
        Optional<DeskDTO> deskDTO = deskService.findOne(id);
        return ResponseUtil.wrapOrNotFound(deskDTO);
    }

    /**
     * DELETE  /desks/:id : delete the "id" desk.
     *
     * @param id the id of the deskDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/desks/{id}")
    @Timed
    public ResponseEntity<Void> deleteDesk(@PathVariable Long id) {
        log.debug("REST request to delete Desk : {}", id);
        deskService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/desks?query=:query : search for the desk corresponding
     * to the query.
     *
     * @param query the query of the desk search
     * @return the result of the search
     */
    @GetMapping("/_search/desks")
    @Timed
    public List<DeskDTO> searchDesks(@RequestParam String query) {
        log.debug("REST request to search Desks for query {}", query);
        return deskService.search(query);
    }

}
