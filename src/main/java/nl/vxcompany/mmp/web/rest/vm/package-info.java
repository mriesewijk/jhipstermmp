/**
 * View Models used by Spring MVC REST controllers.
 */
package nl.vxcompany.mmp.web.rest.vm;
