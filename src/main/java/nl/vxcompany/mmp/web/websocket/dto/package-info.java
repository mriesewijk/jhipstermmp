/**
 * Data Access Objects used by WebSocket services.
 */
package nl.vxcompany.mmp.web.websocket.dto;
