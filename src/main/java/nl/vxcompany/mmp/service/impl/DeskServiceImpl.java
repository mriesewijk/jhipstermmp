package nl.vxcompany.mmp.service.impl;

import nl.vxcompany.mmp.service.DeskService;
import nl.vxcompany.mmp.domain.Desk;
import nl.vxcompany.mmp.repository.DeskRepository;
import nl.vxcompany.mmp.repository.search.DeskSearchRepository;
import nl.vxcompany.mmp.service.dto.DeskDTO;
import nl.vxcompany.mmp.service.mapper.DeskMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Desk.
 */
@Service
@Transactional
public class DeskServiceImpl implements DeskService {

    private final Logger log = LoggerFactory.getLogger(DeskServiceImpl.class);

    private final DeskRepository deskRepository;

    private final DeskMapper deskMapper;

    private final DeskSearchRepository deskSearchRepository;

    public DeskServiceImpl(DeskRepository deskRepository, DeskMapper deskMapper, DeskSearchRepository deskSearchRepository) {
        this.deskRepository = deskRepository;
        this.deskMapper = deskMapper;
        this.deskSearchRepository = deskSearchRepository;
    }

    /**
     * Save a desk.
     *
     * @param deskDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DeskDTO save(DeskDTO deskDTO) {
        log.debug("Request to save Desk : {}", deskDTO);
        Desk desk = deskMapper.toEntity(deskDTO);
        desk = deskRepository.save(desk);
        DeskDTO result = deskMapper.toDto(desk);
        deskSearchRepository.save(desk);
        return result;
    }

    /**
     * Get all the desks.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DeskDTO> findAll() {
        log.debug("Request to get all Desks");
        return deskRepository.findAll().stream()
            .map(deskMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one desk by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DeskDTO> findOne(Long id) {
        log.debug("Request to get Desk : {}", id);
        return deskRepository.findById(id)
            .map(deskMapper::toDto);
    }

    /**
     * Delete the desk by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Desk : {}", id);
        deskRepository.deleteById(id);
        deskSearchRepository.deleteById(id);
    }

    /**
     * Search for the desk corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DeskDTO> search(String query) {
        log.debug("Request to search Desks for query {}", query);
        return StreamSupport
            .stream(deskSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(deskMapper::toDto)
            .collect(Collectors.toList());
    }
}
