package nl.vxcompany.mmp.service.mapper;

import nl.vxcompany.mmp.domain.*;
import nl.vxcompany.mmp.service.dto.DeskDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Desk and its DTO DeskDTO.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class, RoomMapper.class})
public interface DeskMapper extends EntityMapper<DeskDTO, Desk> {

    @Mapping(source = "occupant.id", target = "occupantId")
    @Mapping(source = "room.id", target = "roomId")
    DeskDTO toDto(Desk desk);

    @Mapping(source = "occupantId", target = "occupant")
    @Mapping(source = "roomId", target = "room")
    Desk toEntity(DeskDTO deskDTO);

    default Desk fromId(Long id) {
        if (id == null) {
            return null;
        }
        Desk desk = new Desk();
        desk.setId(id);
        return desk;
    }
}
