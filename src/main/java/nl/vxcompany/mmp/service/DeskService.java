package nl.vxcompany.mmp.service;

import nl.vxcompany.mmp.service.dto.DeskDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Desk.
 */
public interface DeskService {

    /**
     * Save a desk.
     *
     * @param deskDTO the entity to save
     * @return the persisted entity
     */
    DeskDTO save(DeskDTO deskDTO);

    /**
     * Get all the desks.
     *
     * @return the list of entities
     */
    List<DeskDTO> findAll();


    /**
     * Get the "id" desk.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DeskDTO> findOne(Long id);

    /**
     * Delete the "id" desk.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the desk corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @return the list of entities
     */
    List<DeskDTO> search(String query);
}
