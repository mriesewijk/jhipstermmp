package nl.vxcompany.mmp.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Desk entity.
 */
public class DeskDTO implements Serializable {

    private Long id;

    private String deskName;

    private String qrCode;

    private Long occupantId;

    private Long roomId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeskName() {
        return deskName;
    }

    public void setDeskName(String deskName) {
        this.deskName = deskName;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Long getOccupantId() {
        return occupantId;
    }

    public void setOccupantId(Long employeeId) {
        this.occupantId = employeeId;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeskDTO deskDTO = (DeskDTO) o;
        if (deskDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), deskDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DeskDTO{" +
            "id=" + getId() +
            ", deskName='" + getDeskName() + "'" +
            ", qrCode='" + getQrCode() + "'" +
            ", occupant=" + getOccupantId() +
            ", room=" + getRoomId() +
            "}";
    }
}
