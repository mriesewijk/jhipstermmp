package nl.vxcompany.mmp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Desk.
 */
@Entity
@Table(name = "desk")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "desk")
public class Desk implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "desk_name")
    private String deskName;

    @Column(name = "qr_code")
    private String qrCode;

    @OneToOne
    @JoinColumn(unique = true)
    private Employee occupant;

    @ManyToOne
    @JsonIgnoreProperties("desks")
    private Room room;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeskName() {
        return deskName;
    }

    public Desk deskName(String deskName) {
        this.deskName = deskName;
        return this;
    }

    public void setDeskName(String deskName) {
        this.deskName = deskName;
    }

    public String getQrCode() {
        return qrCode;
    }

    public Desk qrCode(String qrCode) {
        this.qrCode = qrCode;
        return this;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Employee getOccupant() {
        return occupant;
    }

    public Desk occupant(Employee employee) {
        this.occupant = employee;
        return this;
    }

    public void setOccupant(Employee employee) {
        this.occupant = employee;
    }

    public Room getRoom() {
        return room;
    }

    public Desk room(Room room) {
        this.room = room;
        return this;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Desk desk = (Desk) o;
        if (desk.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), desk.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Desk{" +
            "id=" + getId() +
            ", deskName='" + getDeskName() + "'" +
            ", qrCode='" + getQrCode() + "'" +
            "}";
    }
}
