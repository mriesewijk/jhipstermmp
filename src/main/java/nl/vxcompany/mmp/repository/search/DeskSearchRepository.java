package nl.vxcompany.mmp.repository.search;

import nl.vxcompany.mmp.domain.Desk;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Desk entity.
 */
public interface DeskSearchRepository extends ElasticsearchRepository<Desk, Long> {
}
