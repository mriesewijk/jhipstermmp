export interface IEmployeeMySuffix {
    id?: number;
    name?: string;
}

export class EmployeeMySuffix implements IEmployeeMySuffix {
    constructor(public id?: number, public name?: string) {}
}
