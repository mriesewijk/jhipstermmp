export interface IDeskMySuffix {
    id?: number;
    deskName?: string;
    qrCode?: string;
    occupantId?: number;
    roomId?: number;
}

export class DeskMySuffix implements IDeskMySuffix {
    constructor(public id?: number, public deskName?: string, public qrCode?: string, public occupantId?: number, public roomId?: number) {}
}
