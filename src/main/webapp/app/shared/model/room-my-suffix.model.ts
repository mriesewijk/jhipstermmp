import { IDeskMySuffix } from 'app/shared/model//desk-my-suffix.model';

export interface IRoomMySuffix {
    id?: number;
    roomName?: string;
    floor?: number;
    desks?: IDeskMySuffix[];
}

export class RoomMySuffix implements IRoomMySuffix {
    constructor(public id?: number, public roomName?: string, public floor?: number, public desks?: IDeskMySuffix[]) {}
}
