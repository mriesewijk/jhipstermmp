import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JhipstermmpEmployeeMySuffixModule } from './employee-my-suffix/employee-my-suffix.module';
import { JhipstermmpRoomMySuffixModule } from './room-my-suffix/room-my-suffix.module';
import { JhipstermmpDeskMySuffixModule } from './desk-my-suffix/desk-my-suffix.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        JhipstermmpEmployeeMySuffixModule,
        JhipstermmpRoomMySuffixModule,
        JhipstermmpDeskMySuffixModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipstermmpEntityModule {}
