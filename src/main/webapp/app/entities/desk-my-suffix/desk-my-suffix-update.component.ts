import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IDeskMySuffix } from 'app/shared/model/desk-my-suffix.model';
import { DeskMySuffixService } from './desk-my-suffix.service';
import { IEmployeeMySuffix } from 'app/shared/model/employee-my-suffix.model';
import { EmployeeMySuffixService } from 'app/entities/employee-my-suffix';
import { IRoomMySuffix } from 'app/shared/model/room-my-suffix.model';
import { RoomMySuffixService } from 'app/entities/room-my-suffix';

@Component({
    selector: 'jhi-desk-my-suffix-update',
    templateUrl: './desk-my-suffix-update.component.html'
})
export class DeskMySuffixUpdateComponent implements OnInit {
    private _desk: IDeskMySuffix;
    isSaving: boolean;

    occupants: IEmployeeMySuffix[];

    rooms: IRoomMySuffix[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private deskService: DeskMySuffixService,
        private employeeService: EmployeeMySuffixService,
        private roomService: RoomMySuffixService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ desk }) => {
            this.desk = desk;
        });
        this.employeeService.query({ filter: 'desk-is-null' }).subscribe(
            (res: HttpResponse<IEmployeeMySuffix[]>) => {
                if (!this.desk.occupantId) {
                    this.occupants = res.body;
                } else {
                    this.employeeService.find(this.desk.occupantId).subscribe(
                        (subRes: HttpResponse<IEmployeeMySuffix>) => {
                            this.occupants = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.roomService.query().subscribe(
            (res: HttpResponse<IRoomMySuffix[]>) => {
                this.rooms = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.desk.id !== undefined) {
            this.subscribeToSaveResponse(this.deskService.update(this.desk));
        } else {
            this.subscribeToSaveResponse(this.deskService.create(this.desk));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IDeskMySuffix>>) {
        result.subscribe((res: HttpResponse<IDeskMySuffix>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackEmployeeById(index: number, item: IEmployeeMySuffix) {
        return item.id;
    }

    trackRoomById(index: number, item: IRoomMySuffix) {
        return item.id;
    }
    get desk() {
        return this._desk;
    }

    set desk(desk: IDeskMySuffix) {
        this._desk = desk;
    }
}
