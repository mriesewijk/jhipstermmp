export * from './desk-my-suffix.service';
export * from './desk-my-suffix-update.component';
export * from './desk-my-suffix-delete-dialog.component';
export * from './desk-my-suffix-detail.component';
export * from './desk-my-suffix.component';
export * from './desk-my-suffix.route';
