import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable } from 'rxjs';
import { DeskMySuffix } from 'app/shared/model/desk-my-suffix.model';
import { DeskMySuffixService } from './desk-my-suffix.service';
import { DeskMySuffixComponent } from './desk-my-suffix.component';
import { DeskMySuffixDetailComponent } from './desk-my-suffix-detail.component';
import { DeskMySuffixUpdateComponent } from './desk-my-suffix-update.component';
import { DeskMySuffixDeletePopupComponent } from './desk-my-suffix-delete-dialog.component';
import { IDeskMySuffix } from 'app/shared/model/desk-my-suffix.model';

@Injectable({ providedIn: 'root' })
export class DeskMySuffixResolve implements Resolve<IDeskMySuffix> {
    constructor(private service: DeskMySuffixService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).map((desk: HttpResponse<DeskMySuffix>) => desk.body);
        }
        return Observable.of(new DeskMySuffix());
    }
}

export const deskRoute: Routes = [
    {
        path: 'desk-my-suffix',
        component: DeskMySuffixComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipstermmpApp.desk.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'desk-my-suffix/:id/view',
        component: DeskMySuffixDetailComponent,
        resolve: {
            desk: DeskMySuffixResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipstermmpApp.desk.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'desk-my-suffix/new',
        component: DeskMySuffixUpdateComponent,
        resolve: {
            desk: DeskMySuffixResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipstermmpApp.desk.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'desk-my-suffix/:id/edit',
        component: DeskMySuffixUpdateComponent,
        resolve: {
            desk: DeskMySuffixResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipstermmpApp.desk.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const deskPopupRoute: Routes = [
    {
        path: 'desk-my-suffix/:id/delete',
        component: DeskMySuffixDeletePopupComponent,
        resolve: {
            desk: DeskMySuffixResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipstermmpApp.desk.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
