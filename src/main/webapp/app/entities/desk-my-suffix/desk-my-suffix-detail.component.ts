import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDeskMySuffix } from 'app/shared/model/desk-my-suffix.model';

@Component({
    selector: 'jhi-desk-my-suffix-detail',
    templateUrl: './desk-my-suffix-detail.component.html'
})
export class DeskMySuffixDetailComponent implements OnInit {
    desk: IDeskMySuffix;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ desk }) => {
            this.desk = desk;
        });
    }

    previousState() {
        window.history.back();
    }
}
