import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDeskMySuffix } from 'app/shared/model/desk-my-suffix.model';
import { DeskMySuffixService } from './desk-my-suffix.service';

@Component({
    selector: 'jhi-desk-my-suffix-delete-dialog',
    templateUrl: './desk-my-suffix-delete-dialog.component.html'
})
export class DeskMySuffixDeleteDialogComponent {
    desk: IDeskMySuffix;

    constructor(private deskService: DeskMySuffixService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.deskService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'deskListModification',
                content: 'Deleted an desk'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-desk-my-suffix-delete-popup',
    template: ''
})
export class DeskMySuffixDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ desk }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(DeskMySuffixDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.desk = desk;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
