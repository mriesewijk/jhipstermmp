import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDeskMySuffix } from 'app/shared/model/desk-my-suffix.model';

type EntityResponseType = HttpResponse<IDeskMySuffix>;
type EntityArrayResponseType = HttpResponse<IDeskMySuffix[]>;

@Injectable({ providedIn: 'root' })
export class DeskMySuffixService {
    private resourceUrl = SERVER_API_URL + 'api/desks';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/desks';

    constructor(private http: HttpClient) {}

    create(desk: IDeskMySuffix): Observable<EntityResponseType> {
        return this.http.post<IDeskMySuffix>(this.resourceUrl, desk, { observe: 'response' });
    }

    update(desk: IDeskMySuffix): Observable<EntityResponseType> {
        return this.http.put<IDeskMySuffix>(this.resourceUrl, desk, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IDeskMySuffix>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IDeskMySuffix[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IDeskMySuffix[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
