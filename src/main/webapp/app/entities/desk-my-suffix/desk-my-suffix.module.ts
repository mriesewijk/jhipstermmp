import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipstermmpSharedModule } from 'app/shared';
import {
    DeskMySuffixComponent,
    DeskMySuffixDetailComponent,
    DeskMySuffixUpdateComponent,
    DeskMySuffixDeletePopupComponent,
    DeskMySuffixDeleteDialogComponent,
    deskRoute,
    deskPopupRoute
} from './';

const ENTITY_STATES = [...deskRoute, ...deskPopupRoute];

@NgModule({
    imports: [JhipstermmpSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        DeskMySuffixComponent,
        DeskMySuffixDetailComponent,
        DeskMySuffixUpdateComponent,
        DeskMySuffixDeleteDialogComponent,
        DeskMySuffixDeletePopupComponent
    ],
    entryComponents: [
        DeskMySuffixComponent,
        DeskMySuffixUpdateComponent,
        DeskMySuffixDeleteDialogComponent,
        DeskMySuffixDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipstermmpDeskMySuffixModule {}
