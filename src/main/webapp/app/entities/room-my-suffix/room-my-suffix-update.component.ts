import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IRoomMySuffix } from 'app/shared/model/room-my-suffix.model';
import { RoomMySuffixService } from './room-my-suffix.service';

@Component({
    selector: 'jhi-room-my-suffix-update',
    templateUrl: './room-my-suffix-update.component.html'
})
export class RoomMySuffixUpdateComponent implements OnInit {
    private _room: IRoomMySuffix;
    isSaving: boolean;

    constructor(private roomService: RoomMySuffixService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ room }) => {
            this.room = room;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.room.id !== undefined) {
            this.subscribeToSaveResponse(this.roomService.update(this.room));
        } else {
            this.subscribeToSaveResponse(this.roomService.create(this.room));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRoomMySuffix>>) {
        result.subscribe((res: HttpResponse<IRoomMySuffix>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get room() {
        return this._room;
    }

    set room(room: IRoomMySuffix) {
        this._room = room;
    }
}
