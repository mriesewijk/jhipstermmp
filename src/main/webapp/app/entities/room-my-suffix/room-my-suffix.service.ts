import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRoomMySuffix } from 'app/shared/model/room-my-suffix.model';

type EntityResponseType = HttpResponse<IRoomMySuffix>;
type EntityArrayResponseType = HttpResponse<IRoomMySuffix[]>;

@Injectable({ providedIn: 'root' })
export class RoomMySuffixService {
    private resourceUrl = SERVER_API_URL + 'api/rooms';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/rooms';

    constructor(private http: HttpClient) {}

    create(room: IRoomMySuffix): Observable<EntityResponseType> {
        return this.http.post<IRoomMySuffix>(this.resourceUrl, room, { observe: 'response' });
    }

    update(room: IRoomMySuffix): Observable<EntityResponseType> {
        return this.http.put<IRoomMySuffix>(this.resourceUrl, room, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IRoomMySuffix>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IRoomMySuffix[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IRoomMySuffix[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
