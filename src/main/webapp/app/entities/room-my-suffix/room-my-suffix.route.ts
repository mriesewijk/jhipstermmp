import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable } from 'rxjs';
import { RoomMySuffix } from 'app/shared/model/room-my-suffix.model';
import { RoomMySuffixService } from './room-my-suffix.service';
import { RoomMySuffixComponent } from './room-my-suffix.component';
import { RoomMySuffixDetailComponent } from './room-my-suffix-detail.component';
import { RoomMySuffixUpdateComponent } from './room-my-suffix-update.component';
import { RoomMySuffixDeletePopupComponent } from './room-my-suffix-delete-dialog.component';
import { IRoomMySuffix } from 'app/shared/model/room-my-suffix.model';

@Injectable({ providedIn: 'root' })
export class RoomMySuffixResolve implements Resolve<IRoomMySuffix> {
    constructor(private service: RoomMySuffixService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).map((room: HttpResponse<RoomMySuffix>) => room.body);
        }
        return Observable.of(new RoomMySuffix());
    }
}

export const roomRoute: Routes = [
    {
        path: 'room-my-suffix',
        component: RoomMySuffixComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipstermmpApp.room.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'room-my-suffix/:id/view',
        component: RoomMySuffixDetailComponent,
        resolve: {
            room: RoomMySuffixResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipstermmpApp.room.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'room-my-suffix/new',
        component: RoomMySuffixUpdateComponent,
        resolve: {
            room: RoomMySuffixResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipstermmpApp.room.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'room-my-suffix/:id/edit',
        component: RoomMySuffixUpdateComponent,
        resolve: {
            room: RoomMySuffixResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipstermmpApp.room.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const roomPopupRoute: Routes = [
    {
        path: 'room-my-suffix/:id/delete',
        component: RoomMySuffixDeletePopupComponent,
        resolve: {
            room: RoomMySuffixResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipstermmpApp.room.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
