export * from './room-my-suffix.service';
export * from './room-my-suffix-update.component';
export * from './room-my-suffix-delete-dialog.component';
export * from './room-my-suffix-detail.component';
export * from './room-my-suffix.component';
export * from './room-my-suffix.route';
