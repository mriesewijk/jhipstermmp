import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRoomMySuffix } from 'app/shared/model/room-my-suffix.model';
import { Principal } from 'app/core';
import { RoomMySuffixService } from './room-my-suffix.service';

@Component({
    selector: 'jhi-room-my-suffix',
    templateUrl: './room-my-suffix.component.html'
})
export class RoomMySuffixComponent implements OnInit, OnDestroy {
    rooms: IRoomMySuffix[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private roomService: RoomMySuffixService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch =
            this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search']
                ? this.activatedRoute.snapshot.params['search']
                : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.roomService
                .search({
                    query: this.currentSearch
                })
                .subscribe(
                    (res: HttpResponse<IRoomMySuffix[]>) => (this.rooms = res.body),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
        }
        this.roomService.query().subscribe(
            (res: HttpResponse<IRoomMySuffix[]>) => {
                this.rooms = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInRooms();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IRoomMySuffix) {
        return item.id;
    }

    registerChangeInRooms() {
        this.eventSubscriber = this.eventManager.subscribe('roomListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
