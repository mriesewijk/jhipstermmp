import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipstermmpSharedModule } from 'app/shared';
import {
    RoomMySuffixComponent,
    RoomMySuffixDetailComponent,
    RoomMySuffixUpdateComponent,
    RoomMySuffixDeletePopupComponent,
    RoomMySuffixDeleteDialogComponent,
    roomRoute,
    roomPopupRoute
} from './';

const ENTITY_STATES = [...roomRoute, ...roomPopupRoute];

@NgModule({
    imports: [JhipstermmpSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RoomMySuffixComponent,
        RoomMySuffixDetailComponent,
        RoomMySuffixUpdateComponent,
        RoomMySuffixDeleteDialogComponent,
        RoomMySuffixDeletePopupComponent
    ],
    entryComponents: [
        RoomMySuffixComponent,
        RoomMySuffixUpdateComponent,
        RoomMySuffixDeleteDialogComponent,
        RoomMySuffixDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipstermmpRoomMySuffixModule {}
