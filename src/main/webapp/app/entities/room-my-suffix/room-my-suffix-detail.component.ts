import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRoomMySuffix } from 'app/shared/model/room-my-suffix.model';

@Component({
    selector: 'jhi-room-my-suffix-detail',
    templateUrl: './room-my-suffix-detail.component.html'
})
export class RoomMySuffixDetailComponent implements OnInit {
    room: IRoomMySuffix;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ room }) => {
            this.room = room;
        });
    }

    previousState() {
        window.history.back();
    }
}
